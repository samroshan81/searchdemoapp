﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace SearchDemoApp
{
    class Program
    {
        public static List<Train> trainList = new List<Train>();
        public static List<Job> jobList = new List<Job>();

        public static object ConsoleTable { get; private set; }

        public static void Main(string[] args)
        {

            jobList.Add(new Job() { JobId = "Kr-2019-100", Titel = "Software Developer", Employer = "Kriminalvarden" });
            jobList.Add(new Job() { JobId = "Pm-2019-101", Titel = "Scrum Master", Employer = "Polismyndigheten" });
            jobList.Add(new Job() { JobId = "Mig-2019-103", Titel = "Project manager", Employer = "Migrationverket" });
            jobList.Add(new Job() { JobId = "Kr-2019-105", Titel = "C# Developer", Employer = "Kriminalvarden" });
            jobList.Add(new Job() { JobId = "Pm-2019-107", Titel = "Front-end", Employer = "Polismyndigheten" });
            jobList.Add(new Job() { JobId = "Mig-2019-109", Titel = "Data analyst", Employer = "Migrationverket" });

            trainList.Add(new Train() { TrainNumber = "SJ3000", Destination = "Stockholm", DepTime = "2019-07-15 12:22:16" });
            trainList.Add(new Train() { TrainNumber = "SJ2000", Destination = "Malmo", DepTime = "2019-07-15 12:22:16" });
            trainList.Add(new Train() { TrainNumber = "SJ1000", Destination = "Goteborg", DepTime = "2019-07-15 12:22:16" });
            trainList.Add(new Train() { TrainNumber = "SJ3500", Destination = "Linkoping", DepTime = "2019-07-15 12:22:16" });
            trainList.Add(new Train() { TrainNumber = "SJ2500", Destination = "Norrkoping", DepTime = "2019-07-15 12:22:16" });
            trainList.Add(new Train() { TrainNumber = "SJ1500", Destination = "Uppsala", DepTime = "2019-07-15 12:22:16" });

            Console.WindowWidth = 90;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(" --------Welcome to Seaarch Demo App!-------\n");

            do
            {

                Console.WriteLine("Please type: \n\r\n J: Job search by job id \r\n T:  Job search by titel \r\n E:  Job search by employer name" 
                                               + "\n\r\n TN: Train search by train number \r\n TD: Train search by destination \r\n DT: Train search by departure time");

                Job job = new Job();
                string input = Console.ReadLine();

                switch (input)
                {
                    case "J":
                        searchbyJobId();
                        break;
                    case "T":
                        searchbyTitel();
                        break;
                    case "E":
                        searchbyEmployer();
                        break;
                    case "TN":
                        searchbyTrainNumber();
                        break;
                    case "TD:":
                        searchbyTrainDestination();
                        break;
                    case "DT":
                        searchbyDepTime();
                        break;
                }

                Console.WriteLine("Do you want to search again?");
            } while (Console.ReadLine().ToUpper() == "YES");
        }

        //Search job methods
        private static void searchbyJobId()
        {
            Console.WriteLine("Please, enter a valid JobId:");
            string input = Console.ReadLine();
            foreach (Job result in jobList.Where(x => x.JobId == input).ToList())
            {
                Console.WriteLine(result);
            }
            
        }
        private static void searchbyTitel()
        {
            Console.WriteLine("Please, enter Job Titel:");
            string input = Console.ReadLine();
            foreach (Job result in jobList.Where(x => x.Titel == input).ToList())
            {
                Console.WriteLine(result);
            }
        }

        private static void searchbyEmployer()

        {
            Console.WriteLine("Please, enter Employer:");
            string input = Console.ReadLine();
            foreach (Job result in jobList.Where(x => x.Employer == input).ToList())
            {
                Console.WriteLine(result);

            }
        }

        //Search train methods
        private static void searchbyTrainNumber()

        {
            Console.WriteLine("Please, enter TrainNumber:");
            string input = Console.ReadLine();
            foreach (Train result in trainList.Where(x => x.TrainNumber == input).ToList())
            {
                Console.WriteLine(result);
            }
        }

        private static void searchbyTrainDestination()

        {
            Console.WriteLine("Please, enter Destination:");
            string input = Console.ReadLine();
            foreach (Train result in trainList.Where(x => x.Destination == input).ToList())
            {
                Console.WriteLine(result);
            }
        }

        private static void searchbyDepTime()

        {
            Console.WriteLine("Please, enter DepTime:");
            string input = Console.ReadLine();
            foreach (Train result in trainList.Where(x => x.DepTime == input).ToList())
            {
                Console.WriteLine(result);
            }
        }



    }
}
