﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SearchDemoApp
{
    public class Train
    {
        public string TrainNumber { get; set; }
        public string Destination { get; set; }
        public string DepTime { get; set; }
       

        public override string ToString()
        {
            string output = String.Format("TrainNumber :{0} \n Destination :{1} \n DepTime :{2} ", this.TrainNumber, this.Destination, this.DepTime);
            return output;
        }
    }
}
