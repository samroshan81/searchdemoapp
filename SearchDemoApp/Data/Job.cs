﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SearchDemoApp
{
    public class Job
    {
        public string JobId { get; set; }
        public string Titel { get; set; }
        public string Employer { get; set; }

        public override string ToString()
        {
            string output = String.Format("JobId :{0} \n Titel :{1} \n Employer :{2}", this.JobId, this.Titel, this.Employer);
            return output;
        }
    }
}
